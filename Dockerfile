FROM alpine
COPY --from=rekgrpth/srcv8 /usr/src/v8 /tmp/v8
RUN exec 2>&1 \
    && set -ex \
    && apk add --no-cache --virtual .build-deps \
        curl \
        g++ \
        git \
        linux-headers \
        make \
        pkgconf \
        python2 \
        python3 \
        samurai \
    && apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing --virtual .edge-deps \
        gn \
    && SRC=/tmp/v8 \
    && ln -fs /usr/bin/gn "$SRC/buildtools/linux64/gn" \
    && cd "$SRC" \
    && tools/dev/v8gen.py -vv x64.release -- \
        clang_use_chrome_plugins=false \
        is_asan=false \
        is_cfi=false \
        is_clang=false \
        is_component_build=true \
        is_desktop_linux=false \
        is_official_build=true \
        proprietary_codecs=false \
        strip_debug_info=true \
        symbol_level=0 \
        target_cpu=\"x64\" \
        target_os=\"linux\" \
        toolkit_views=false \
        treat_warnings_as_errors=false \
        use_aura=false \
        use_custom_libcxx=false \
        use_dbus=false \
        use_gio=false \
        use_glib=false \
        use_gold=false \
        use_ozone=false \
        use_sysroot=false \
        use_udev=false \
        v8_deprecation_warnings=false \
        v8_enable_future=true \
        v8_enable_gdbjit=false \
        v8_enable_i18n_support=false \
        v8_enable_pointer_compression=true \
        v8_imminent_deprecation_warnings=false \
        v8_monolithic=false \
        v8_static_library=false \
        v8_target_cpu=\"x64\" \
        v8_untrusted_code_mitigations=false \
        v8_use_external_startup_data=false \
    && ninja -C out.gn/x64.release/ -j$(nproc) \
    && mkdir -p /usr/local/lib /usr/local/include \
    && cp -r $SRC/include/* /usr/local/include/ \
    && cp $SRC/out.gn/x64.release/lib*.so /usr/local/lib/ \
    && (strip /usr/local/bin/* /usr/local/lib/* || true) \
    && apk add --no-cache --virtual .v8-rundeps \
        $(scanelf --needed --nobanner --format '%n#p' --recursive /usr/local | tr ',' '\n' | sort -u | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }') \
    && apk del --no-cache .build-deps \
    && apk del --no-cache .edge-deps \
    && rm -rf /usr/share/doc /usr/share/man /usr/local/share/doc /usr/local/share/man /tmp/* /var/tmp/* /var/cache/apk/* \
    && echo done
